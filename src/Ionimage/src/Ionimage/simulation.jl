import ..Photofragments: random_particle, I3D

# Simulate a totally projected (non-sliced) ion image.
function totalimage(fv, vmin, vmax, n, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2,
                    height, width, center)
    dummymass = -1.0
    image = zeros(Float64, (height, width))
    totalint = 0.0
    for i in 1:floor(Int, n)
        p = random_particle(dummymass, 1, vmin, vmax)
        intensity = fv(p.v)*I3D(p.Ω, p.Θ, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2)*sind(p.Ω)
        totalint += intensity
        y, x = project(p, center)
        if 0 < y <= height && 0 < x <= width
            @inbounds image[y, x] += intensity
        end
    end
    image ./ totalint .* n
end


# Simulate a sliced ion image.
function slicedimage(fv, vmin, vmax, n, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2,
                     height, width, center, slice)
    dummy = -1.0
    Ωmin, Ωmax = acosd(slice), acosd(-slice)
    image = zeros(Float64, (height, width))
    totalint = 0.0
    for i in 1:floor(Int, n)
        p = random_particle(dummy, 1, vmin, vmax)
        intensity = fv(p.v)*I3D(p.Ω, p.Θ, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2)*sind(p.Ω)
        totalint += intensity
        if !(Ωmin <= p.Ω <= Ωmax)
            continue
        end
        y, x = project(p, center)
        if 0 < y <= height && 0 < x <= width
            @inbounds image[y, x] += intensity
        end
    end
    image ./ totalint .* n
end


# Calculate a projected photofragment position on a screen.
function project(p, center)
    yc, xc = center
    v = p.v
    Ω = p.Ω
    Θ = p.Θ
    y = round(Int, yc - v*sind(Ω)*cosd(Θ), RoundNearestTiesUp)
    x = round(Int, xc + v*sind(Ω)*sind(Θ), RoundNearestTiesUp)
    y, x
end


Gauss(x, xc, σ) = 1/(√(2π)*σ)*exp(-(x - xc)^2/(2*σ^2))


"""
    simulateimg(fv, vmin, vmax, n, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2, size, center, slice)

Simulate an image of n photofragments with the following parameters.
  - fv: speed distribution function
  - vmin: the minimum limit of fragment speed to take into account
  - vmax: the maximum limit of fragment speed to take into account
  - n : number of photofragments to simulate
  - Γ : angle of laser polarization
  - Δ : polar angle of orienting field vector
  - Φ : azimuthal angle of orienting field vector
  - α : angle between the recoil velocity and the permanent dipole moment
  - χ : angle between the recoil velocity and the transition dipole moment
  - ϕ_μd: azimuthal angle of the permanent dipole moment and the transition
          dipole moment around the recoil velocity
  - size: size of screen (height, width) in pixcel
  - center: center coordinate of the projection
  - slice : slicing width fraction to the diameter of Newton sphere
            1.0 for a totally projected image
            0.1 for an ideal (and typical) slice image
"""
function simulateimg(fv, vmin, vmax, n, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2,
                     size, center, slice)
    height, width = size
    image = if slice >= 1.0
        totalimage(fv, vmin, vmax, n, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2,
                   height, width, center)
    else
        slicedimage(fv, vmin, vmax, n, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2,
                    height, width, center, slice)
    end
end

# Simulate an image of n photofragments.
function simulateimg(fv::Function, vmin::Real, vmax::Real, n::Real;
                     Γ=90.0, Δ=180.0, Φ=0.0, α=180.0, χ=0.0, ϕ_μd=0.0,
                     c1=0.0, c2=0.0, size=(501, 501), center=nothing, slice=1.0)
    if center == nothing
        center = @. ceil(size/2)
    end
    simulateimg(fv, vmin, vmax, n, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2, size, center, slice)
end

function simulateimg(v::Real, σv::Real, n::Real; kwargs...)
    if σv ≈ 0.0
        fv = x -> 1
    else
        fv = x -> Gauss(x, v, σv)
    end
    vmin, vmax = v - 3σv, v + 3σv
    simulateimg(fv, vmin, vmax, n; kwargs...)
end
