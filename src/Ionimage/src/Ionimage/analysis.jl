using Statistics

# Return speed and angular distributions of an image
function analyze(image, center, bin, r, θ, size, useβ1::Bool, symmetrize::Bool)
    if size === nothing
        short = minimum(Base.size(image))
        size = (short, short)
    end
    height, width = size
    croppedimage = crop(image, center, height, width)
    croppedcenter = shiftcenter(image, center, height, width)
    dist = distribution(croppedimage, croppedcenter, θ, bin, symmetrize)
    speeddist = getspeeddistribution(dist, θ)
    if r <= 0
        _, r = findmax(speeddist[2])
    end
    angulardist = getangulardistribution(dist, r, θ, useβ1)
    ImageData(image, center, bin, r, θ, size, useβ1, symmetrize, dist,
              angulardist, speeddist)
end

function analyze(file::String, center; bin=3, r=0, θ=0:5:180, size=nothing,
                 useβ1=false, symmetrize=true)
    image = readdlm(file, Float64)
    analyze(image, center, bin, r, θ, size, useβ1, symmetrize)
end

function analyze(image::Array{<:Real,2}, center; bin=3, r=0, θ=0:5:180,
                 size=nothing, useβ1=false, symmetrize=true)
    analyze(image, center, bin, r, θ, size, useβ1, symmetrize)
end


# Derive the maximum radius of analyzed area on an image
function getrmax(image, center, bin)
    Ny, Nx = size(image)
    room = floor(Int, bin/2)
    rmax = min(center[2] - 1, Nx - center[2], center[1] - 1, Ny - center[1])
    rmax = rmax - room
end


# Convert an image I(x, y) into I(r, θ)
function distribution(image, center, θ, bin, symmetrize)
    binning(img, bin, pos) = sum(img[binindexes(pos, bin)...])

    rmax = getrmax(image, center, bin)
    dist = zeros(Float64, (rmax, length(θ)))
    if symmetrize
        for idx = 1:length(θ), r = 1:rmax
            pos1 = polarpos(center, r,  θ[idx])
            pos2 = polarpos(center, r, -θ[idx])
            int1 = binning(image, bin, pos1)
            int2 = binning(image, bin, pos2)
            dist[r, idx] = (int1 + int2)/2
        end
    else
        for idx = 1:length(θ), r = 1:rmax
            pos = polarpos(center, r, θ[idx])
            dist[r, idx] = binning(image, bin, pos)
        end
    end
    dist
end


# Obtain a speed distribution from an image I(r, θ)
function getspeeddistribution(dist, θ)
    rmax = size(dist, 1)
    sinθ = sind.(θ)
    I = zeros(Float64, rmax)
    for i in 1:length(sinθ), r in 1:rmax
        I[r] += dist[r, i]*r*r*sinθ[i]
    end
    collect(1:rmax), I
end


# Calculate a normalize constant from a set of (x, y = y(x))
function normalizeconstant(x, y)
    s = 0.0
    n = length(x)-1
    if n > 0
        for i = 1:n
            s += (y[i] + y[i+1])*(x[i+1] - x[i])/2
        end
    end
    1/s
end


# Fit a angular distribution with Legendre polynomials.
function fit_angular(x, y, useβ1)
    cosx = cosd.(x)  # folding the x range into -1 <= cos(θ) <= 1
    foldedx, foldedy = foldxrange(cosx, y)
    c = normalizeconstant(foldedx, foldedy)
    yn = y .* c
    P1 = cosd.(x)
    P2 = 0.5 .* (3 .* cosd.(x) .^ 2 .- 1)
    if useβ1
        denominator = sum(P1 .* P2) .^ 2 - sum(P1 .^ 2) .* sum(P2 .^ 2)
        β1 = (sum(P2 .^ 2) .* sum(P1 .* (1 - 2 .* yn)) - sum(P1 .* P2) .* sum(P2 .* (1 - 2 .* yn))) ./ denominator
        β2 = (sum(P1 .^ 2) .* sum(P2 .* (1 - 2 .* yn)) - sum(P1 .* P2) .* sum(P1 .* (1 - 2 .* yn))) ./ denominator
    else
        β1 = 0.0
        β2 = (2 .* sum(yn .* P2) - sum(P2)) ./ sum(P2 .^ 2)
    end

    (0.5 .* (1 .+ β1 .* P1 .+ β2 .* P2) ./ c), β1, β2
end


function foldxrange(x, y)
    data = Dict()
    for (xx, yy) in zip(x, y)
        if haskey(data, xx)
            push!(data[xx], yy)
        else
            data[xx] = [yy]
        end
    end

    folded = []
    for (xx, yyarray) in data
        push!(folded, (xx, mean(yyarray)))
    end
    sort!(folded, by = x -> x[1])

    foldedx = []
    foldedy = []
    for (xx, yy) in folded
        push!(foldedx, xx)
        push!(foldedy, yy)
    end
    foldedx, foldedy
end


function getangulardistribution(dist, r, θ, useβ1)
    x = θ
    y = dist[r, :]
    fit, β1, β2 = fit_angular(x, y, useβ1)
    c = normalizeconstant(x, y)
    yn = y .* c .* (maximum(θ) - minimum(θ)) .* 0.5
    fitn = fit .* c .* (maximum(θ) - minimum(θ)) .* 0.5
    raw = (x, y, fit)
    norm = (x, yn, fitn)
    AngularDistribution(raw, norm, β1, β2)
end


# Process shear mapping for an image and return the new image
function shear(img, θ)
    height,width=size(img)
    yc,xc=ceil.(Int, [height,width] ./ 2)
    newimg=zeros(Float64,(height,width))
    for x in 1:width, y in 1:height
        if y == yc
            newimg[y,x]+=img[y,x]
            continue
        end
        xnew=round(Int, (y-yc)*tand(θ) + x)
        if 1<=xnew<=width
            newimg[y,xnew]+=img[y,x]
        end
    end
    newimg
end
