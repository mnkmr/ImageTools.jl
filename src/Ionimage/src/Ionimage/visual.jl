using Printf

const colormap_white = ((-1.0,  0.0, 0.0, 0.0),
                        (-0.25, 1.0, 0.0, 0.0),
                        ( 0.0,  1.0, 1.0, 1.0),
                        ( 0.25, 0.0, 0.0, 1.0),
                        ( 1.0,  0.0, 0.0, 0.0))
const colormap_black = ((-1.0,  1.0, 0.5, 0.5),
                        (-0.25, 1.0, 0.0, 0.0),
                        ( 0.0,  0.0, 0.0, 0.0),
                        ( 0.25, 0.0, 0.0, 1.0),
                        ( 1.0,  0.5, 0.5, 1.0))
const colormap_default = ((0.0, 1.0, 1.0, 1.0),
                          (0.2, 0.0, 0.0, 1.0),
                          (0.4, 0.0, 1.0, 1.0),
                          (0.6, 0.0, 1.0, 0.0),
                          (0.8, 1.0, 1.0, 0.0),
                          (1.0, 1.0, 0.0, 0.0))


struct Colormap
    c::Char
    map
end


function develop(raw, colormap, depth)
    if depth == nothing
        depth = getdepth(raw)
    end
    Ny, Nx = size(raw)
    img = zeros(Float64, (Ny, Nx, 3))
    I = raw ./ depth
    I[I .> 1.0] .= 1.0
    colormapr = Colormap('r', map(x -> (x[1], x[2]), colormap))
    colormapg = Colormap('g', map(x -> (x[1], x[3]), colormap))
    colormapb = Colormap('b', map(x -> (x[1], x[4]), colormap))
    img[:, :, 1] = map(x -> gencolor(x, colormapr), I)
    img[:, :, 2] = map(x -> gencolor(x, colormapg), I)
    img[:, :, 3] = map(x -> gencolor(x, colormapb), I)
    img
end

function develop(raw, colormap::AbstractString, depth)
    if colormap == "white"
        cmap = colormap_white
    elseif colormap == "black"
        cmap = colormap_black
    else
        cmap = colormap_default
    end
    develop(raw, cmap, depth)
end

"""
    develop(raw, colormap, depth)

Make a false-colorized matrix [m, n, 3] from an image [m, n]
Available colormap is either "white", "black", or "default"
"""
function develop(raw; colormap="default", depth=nothing)
    develop(raw, colormap, depth)
end

"""
    develop(raw, colormap, depth)

Make a false-colorized matrix [m, n, 3] from an Ionimage.ImageData
Available colormap is either "white", "black", or "default"
"""
function develop(imagedata::ImageData; colormap="default", depth=nothing, hint=false)
    height, width = imagedata.size
    croppedimage = crop(imagedata.image, imagedata.center, height, width)
    img = develop(croppedimage, colormap, depth)
    if hint
        newcenter = shiftcenter(imagedata.image, imagedata.center, height, width)
        img = pointimage!(img, newcenter, imagedata.bin, imagedata.r, imagedata.θ, imagedata.symmetrize)
    end
    img
end


const depthsteps = (8, 15, 31, 63, 127, 255, 511, 1023, 2047)


# Return a reference image depth
function getdepth(image)
    maxdepth = maximum(abs.(image))
    depth = maxdepth
    for step in depthsteps
        if step >= maxdepth
            depth = step
            break
        end
    end
    println(@sprintf("The color depth is set to %d steps.", depth))
    return depth
end


is_in(c, s) = findfirst(c, s) != nothing


function incrementcolor(i, I)
    min(1.0, i + I)
end


function colorize!(img, bin, pos, rgb::AbstractString, I)
    yc, xc = binindexes(pos, bin)
    is_in("r", rgb) && (img[yc, xc, 1] .= incrementcolor.(img[yc, xc, 1], I))
    is_in("g", rgb) && (img[yc, xc, 2] .= incrementcolor.(img[yc, xc, 2], I))
    is_in("b", rgb) && (img[yc, xc, 3] .= incrementcolor.(img[yc, xc, 3], I))
    img
end

function colorize!(img, bin, pos, rgb::AbstractString)
    colorize!(img, bin, pos, rgb, 0.5)
end


function decolorize!(img, bin, pos, rgb::AbstractString, P)
    yc, xc = binindexes(pos, bin)
    is_in("r", rgb) && (img[yc, xc, 1] .= img[yc, xc, 1] .* (1.0 - P))
    is_in("g", rgb) && (img[yc, xc, 2] .= img[yc, xc, 2] .* (1.0 - P))
    is_in("b", rgb) && (img[yc, xc, 3] .= img[yc, xc, 3] .* (1.0 - P))
    img
end

function decolorize!(img, bin, pos, rgb::AbstractString)
    decolorize!(img, bin, pos, rgb, 0.5)
end


function pointimage!(img, center, bin, r, θarray, symmetrize)
    # point the center
    decolorize!(img, 5, center, "rgb")
    colorize!(img, 5, center, "rg")
    # point the bins
    if symmetrize
        for θ in θarray
            pos = polarpos(center, r, θ)
            decolorize!(img, bin, pos, "rgb")
            colorize!(img, bin, pos, "g")
            if θ != 0 && θ != 180
                pos = polarpos(center, r, -θ)
                decolorize!(img, bin, pos, "rgb")
                colorize!(img, bin, pos, "g")
            end
        end
    else
        for θ in θarray
            pos = polarpos(center, r, θ)
            decolorize!(img, bin, pos, "rgb")
            colorize!(img, bin, pos, "g")
        end
    end
    img
end


function gencolor(z, colormap)
    zi = 0
    zf = 0
    ci = nothing
    cf = nothing
    for (zref, cref) in colormap.map
        zi = zf
        zf = zref
        ci = cf
        cf = cref
        if zref >= z
            break
        end
    end
    if ci == nothing || ci == cf
        return cf
    end
    ci + (z - zi)/(zf - zi)*(cf - ci)
end
