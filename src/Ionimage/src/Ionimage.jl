module Ionimage

export crop, shiftcenter
export analyze, shear
export simulateimg
export develop

"""
    AngularDistribution

Type for a angular distribution.
`raw` is a tuple of (x, y, fit).
`norm` is a tuple of (x, yn, fitn).
x is the angle vector.
y and fit are not normalized.
yn and fitn are normalized.
β1 and β2 are the fitting coefficients associated with the first and second
Legendre polynomial, respectively.
"""
struct AngularDistribution
    raw::Tuple{Vector{Float64},Vector{Float64},Vector{Float64}}
    norm::Tuple{Vector{Float64},Vector{Float64},Vector{Float64}}
    β1::Float64
    β2::Float64
end


"""
    ImageData

Type for a result of an image analysis.
"""
struct ImageData
    image
    center
    bin
    r
    θ
    size
    useβ1::Bool
    symmetrize::Bool
    dist::Array{Float64,2}
    ang::AngularDistribution
    spd::Tuple{Vector{Float64},Vector{Float64}}
end


# Calculate a Cartesian position (x, y) from a polar position (r, θ).
function polarpos(center, r, θ)
    center[1] - r*cosd(θ), center[2] + r*sind(θ)
end


# Crop image with a center coordinate and (height, width)
function crop(image, center, height, width)
    Ny, Nx = size(image)
    yrange = imageindexes(center[1], height, Ny)
    xrange = imageindexes(center[2], width, Nx)
    image = image[yrange, xrange]
end

function crop(imgdata::ImageData)
    height, width = imgdata.size
    crop(imgdata.image, imgdata.center, height, width)
end


# Derive the new center coordinate of a image cropped by crop()
function shiftcenter(image, center, height, width)
    Ny, Nx = size(image)
    yrange = imageindexes(center[1], height, Ny)
    xrange = imageindexes(center[2], width, Nx)
    yc = center[1] - yrange[1] + 1
    xc = center[2] - xrange[1] + 1
    yc, xc
end


# Try to derive indexes I which length(I) ≤ len && minimum(I) ≤ 1 && maximum(I) ≤ xmax
function imageindexes(xc, len, xmax)
    d = len/2
    xi = max(xc - floor(Int, d), 1)
    xf = min(xc + floor(Int, d), xmax)
    xi:xf
end


# Derive a indexes of a bin
function binindexes(pos, bin)
    x = round(Int, pos[1])
    y = round(Int, pos[2])
    b = floor(Int, bin/2)
    x-b:x+b, y-b:y+b
end

include("Ionimage/analysis.jl")
include("Ionimage/simulation.jl")
include("Ionimage/visual.jl")


end # module
