module PBasexCache

function __init__()
    # The directory of G⁺ matrix cache files
    if !haskey(ENV, "PBASEX_CACHEDIR")
        ENV["PBASEX_CACHEDIR"] = joinpath(dirname(@__FILE__), "..", "cache")
    end
    nothing
end

end # module
