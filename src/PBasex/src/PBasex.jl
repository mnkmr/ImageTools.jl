"""
    PBasex
This package provides the functions to reconstruct a 3-dimensional distribution
from the corresponding projected 2-dimensional image using **pBasex** method.
[G. A. Garcia et al., *Rev. Sci. Instrum.* **75**, 2004, 4989-4996.]

# Usage
```julia-repl
julia> using PBasex

julia> pbasex = PBasexReconstructor(400)  # Build a reconstructor
PBasex.PBasexReconstructor[400x400]: l=[0, 2], σ=2

julia> ret = pbasex(img)  # Reconstruct an projection image
PBasex.Reconstructedimage: raw: 600x600, slice: 400x400

julia> ret2 = pbasex(img, [210, 210])  # Shift the center of target region
PBasex.Reconstructedimage: raw: 600x600, slice: 400x400
```

# Save & Load a reconstructor
```julia-repl
julia> using PBasex

julia> pbasex = PBasexReconstructor(400)  # Build a reconstructor
PBasex.PBasexReconstructor[400x400]: l=[0, 2], σ=2

julia> savercs("pbasex_400.jld2", pbasex)

julia> pbasex_loaded = loadrcs("pbasex_400.jld2")
PBasex.PBasexReconstructor[400x400]: l=[0, 2], σ=2
```
"""
module PBasex

export pbasex, PBasexReconstructor, savercs, loadrcs

import Dierckx: Spline2D
import LinearAlgebra: svd, Diagonal
import ProgressMeter: Progress, next!
import QuadGK: quadgk


"""
ReconstructedImage

A reconstructed image and concomitant information.
Given that `recons` is an instance of this type:

  recons.slice: The center slice of the reconstructed 3D distribution.
  recons.F    : The reconstructed 3D distribution F(R, θ).
  recons.β    : The radial dependence of anisotropy parameters.
                Specify the order of corresponding Legendre polynomials,
                for example, recons.β[2] returns that of 2nd order.
  recons.I    : The radial distribution.
  recons.R    : The radius vector in pixel unit.
  recons.θ    : The angle vector in degree unit.
  recons.raw  : The raw image given at the entrance.
  recons.len  : The length of a side edge of `recons.slice`.
  recons.θstep: The angular interval of `recons.θ`.
  recons.l    : The orders of Legendre polynomials employed for basis set.
  recons.σ    : The width parameter of basis-set functions.

The speed distribution of fragments is visualized to plot 'recons.I' v.s.
`recons.R`.

The radial dependence of anisotropy parameter is visualized to plot
`recons.β[l]` v.s. `recons.R`. valid `l`s are listed in `recons.l`.
Note that β[l][k] is not so meaningful if `recons.I[k] ≈ 0`.
"""
struct ReconstructedImage
    slice::Array{Float64,2}
    F::Array{Float64,2}
    β::Dict{Int,Array{Float64,1}}
    I::Array{Float64,1}
    R::Array{Float64,1}
    θ::Array{Float64,1}

    # ReconstructedImage should be self-descriptive
    raw::Array{Float64,2}
    len::Int
    θstep::Float64
    l::Array{Int,1}
    σ::Int
end


# Return the center coordinate (yc, xc) of the image
function calccenter(img)
    map(x -> (x + 1)/2, size(img))
end


# Return the maximum radius included in the image with the center
function calcRmax(img, center)
    zc, xc = center
    nz, nx = size(img)
    min(zc - 1, xc - 1, nz - zc, nx - xc)
end


# Return the grid in a polar coordinate
function polarcoordinates(len::Int, θstep::Float64)
    R₁ = iseven(len) ? 0.5 : 1.0
    R = R₁:len/2
    θ = 0.0:θstep:180.0
    R′ = R  # This line assumes that the whole Newton sphere is captured in the image
    θ′ = θ
    R, θ, R′, θ′
end


# Calculate Legendre polynomials Pₗ(x)
function legendre(l::Int, x::Real)::Float64
    if l == 0
        return Float64(1)
    elseif l == 1
        return Float64(x)
    elseif l == 2
        return (3*x^2 - 1)/2
    elseif l == 3
        return (5*x^3 - 3*x)/2
    elseif l == 4
        return (35*x^4 - 30*x^2 + 3)/8
    elseif l == 5
        return (63*x^5 - 70*x^3 + 15*x)/8
    elseif l == 6
        return (231*x^6 - 315*x^4 + 105*x^2 - 5)/16
    elseif l == 7
        return (429*x^7 - 693*x^5 + 315*x^3 - 35*x)/16
    elseif l == 8
        return (6435*x^8 - 12012*x^6 + 6930*x^4 - 1260*x^2 + 35)/128
    end

    Pₙ = legendre(7, x)
    Pₙ₊₁ = legendre(8, x)
    for n in 8:(l - 1)
        Pₙ₋₁ = Pₙ
        Pₙ = Pₙ₊₁
        Pₙ₊₁ = ((2n + 1)*x*Pₙ - n*Pₙ₋₁)/(n + 1)
    end
    Pₙ₊₁
end


# The basis function (Eq. 2)
function fₖₗ(R::Float64, Rₖ::Float64, σ::Int, l::Int, cosθ::Float64)
    ΔR = R - Rₖ
    exp(-(ΔR*ΔR)/σ)*legendre(l, cosθ)
end


# Return the integrand of Eq. 4
function integrand(x::Float64, z::Float64, Rₖ::Float64, l::Int, σ::Int)
    function f(r::Float64)
        R = sqrt(z*z + r*r)
        cosθ = z/R
        r*fₖₗ(R, Rₖ, σ, l, cosθ)/sqrt(r*r - x*x)
    end
end


# A slight shift to avoid a singular point at x == r
# FIXME: What would be the appropriate number?
const Δx = 1e-12

# Compute the elements of G matrix by Eq.4
function gmatrix(len::Int, θstep::Float64, l, σ::Int, verbose::Bool)
    R, _, R′, θ′ = polarcoordinates(len, θstep)
    Rmax = last(R)
    imax = length(R′)
    jmax = length(θ′)
    kmax = length(R)
    # G is symmetric along j if l is even and is asymmetric if l is odd
    jmid = ceil(Int, jmax/2)

    # Set the origin (0, 0) as the root of R′ vector
    # R′ points the vertical top when θ′ = 0°
    x = R′*transpose(sind.(θ′))
    z = R′*transpose(cosd.(θ′))

    p = Progress(kmax*length(l), 1, "Computing basis set...")
    G = zeros(Float64, imax*jmax, kmax*length(l))
    for lidx in 1:length(l)
        sign = iseven(l[lidx]) ? 1 : -1
        for k in 1:kmax
            kl = (k - 1)*length(l) + lidx
            for j in 1:jmid, i in 1:imax
                @inbounds f = integrand(x[i, j], z[i, j], R[k], l[lidx], σ)
                g, _ = quadgk(f, abs(x[i, j])+Δx, Rmax + 3σ)::Tuple{Float64,Float64}

                ij  = (i - 1)*jmax + j
                ij′ = (i - 1)*jmax + (jmax - j + 1)
                @inbounds G[ij,  kl] = 2*g
                @inbounds G[ij′, kl] = sign*2*g
            end
            verbose && next!(p)  # update progress meter
        end
    end
    G
end


# Compute SVD decomposition of G matrix and return the pseudo inverse matrix G⁺
function pseudoinverse(G)
    F = svd(G)
    F.V*Diagonal(1 ./ F.S)*transpose(F.U)
end


# Return a "sanitized" G⁺ matrix.
#
# In principle, G⁺[kl, ij] == sign*G⁺[kl, ij′] should be true with an arbitrary
# (i, j, k, l). However, there is a slight error. Take average to minimize it.
function balance(G⁺, len::Int, θstep::Float64, l)
    R, θ, R′, θ′ = polarcoordinates(len, θstep)
    imax = length(R′)
    jmax = length(θ′)
    kmax = length(R)
    jmid = ceil(Int, jmax/2)
    for lidx in 1:length(l)
        sign = ifelse(iseven(l[lidx]), 1, -1)
        for k in 1:kmax
            kl = (k - 1)*length(l) + lidx
            for j in 1:jmid, i in 1:imax
                ij  = (i - 1)*jmax + j
                ij′ = (i - 1)*jmax + (jmax - j + 1)
                @inbounds g⁺ = (G⁺[kl, ij] + sign*G⁺[kl, ij′])/2
                @inbounds G⁺[kl, ij] = g⁺
                @inbounds G⁺[kl, ij′] = sign*g⁺
            end
        end
    end
    G⁺
end


# Crop an image array
function crop(img, center, Rmax)
    zc, xc = center
    top = round(Int, zc - Rmax)
    bottom = round(Int, zc + Rmax)
    left = round(Int, xc - Rmax)
    right = round(Int, xc + Rmax)
    newcenter = [Rmax + 1, Rmax + 1]
    img[top:bottom, left:right], newcenter
end


# Symmetrize an image array
function symmetrize(img, dim)
    (img .+ reverse(img, dims=dim))./2
end


# Adjust base line forcibly
function setbaselevel(arr::Array{T,N}, base) where {T<:Real,N}
    arr[arr .< base] .= T(base)
    arr
end

function setbaselevel(arr::Array{T,N}, thr::Nothing) where {T<:Real,N}
    arr
end


# Convert a Cartesian coordinate into a polar coordinate
function cartesian_to_polar(cart, center, R′, θ′)
    zc, xc = center
    zref = 1.0:size(cart, 1)
    xref = 1.0:size(cart, 2)
    spline = Spline2D(zref, xref, cart, kx=3, ky=3)
    x = xc .+ R′*transpose(sind.(θ′))
    z = zc .- R′*transpose(cosd.(θ′))
    imax = length(R′)
    jmax = length(θ′)
    polar = zeros(Float64, imax*jmax)
    for i in 1:imax, j in 1:jmax
        ij = (i - 1)*jmax + j
        @inbounds polar[ij] = spline(z[i, j], x[i, j])
    end
    polar
end


# Reconstruct a 3D distribution from C matrix and the basis-set functions
function reconstruct(C, R, θ, l, σ::Int)
    cosθ = cosd.(θ)
    F = zeros(Float64, length(θ), length(R))
    for m in 1:length(R), n in 1:length(θ)
        for k in 1:length(R), lidx in 1:length(l)
            kl = (k - 1)*length(l) + lidx
            # The last R[m] corresponds to Jacobian
            @inbounds F[n, m] += C[kl]*fₖₗ(R[m], R[k], σ, l[lidx], cosθ[n])
        end
    end
    F
end


# Convert a polar coordinate to a Cartesian coordinate
function polar_to_cartesian(polar, R, θ)
    Rmax = last(R)
    height = ceil(Int, 2*Rmax + 1)
    width = ceil(Int, 2*Rmax + 1)
    cart = zeros(Float64, height, width)
    rc = Rmax + 1
    zc = Rmax + 1
    spline = Spline2D(θ, R, polar, kx=3, ky=3)
    for r in 1:width
        r′ = -rc + r
        r′² = r′*r′
        for z in 1:height
            z′ = zc - z
            z′² = z′*z′
            R = sqrt(r′² + z′²)
            if R > Rmax
                continue
            end
            θ = acosd(z′/R)
            @inbounds cart[z, r] = ifelse(R == 0, 0.0, spline(θ, R)*R)
        end
    end
    cart
end


# Compute the radial distribution
function radialdistribution(C, R, l, σ::Int)
    mmax = length(R)
    kmax = length(R)
    I = zeros(Float64, length(R))
    for m in 1:mmax
        for k in 1:kmax
            kl = (k - 1)*length(l) + 1
            @inbounds I[m] += C[kl]*fₖₗ(R[m], R[k], σ, 0, 0.0)
        end
        @inbounds I[m] *= R[m]^2
    end
    I
end


# Compute the anisotropy parameters
function anisotropyparameter(I, C, R, l, lidx::Int, σ::Int)
    mmax = length(R)
    kmax = length(R)
    β = zeros(Float64, length(R))
    for m in 1:mmax
        for k in 1:kmax
            kl = (k - 1)*length(l) + lidx
            @inbounds β[m] += C[kl]*fₖₗ(R[m], R[k], σ, 0, 0.0)
        end
        @inbounds β[m] *= R[m]^2/I[m]
    end
    β
end

function anisotropyparameter(I, C, R, l, σ)
    list = []
    for lidx in 1:length(l)
        β = anisotropyparameter(I, C, R, l, lidx, σ)
        push!(list, (l[lidx], β))
    end
    Dict{Int,Array{Float64,1}}(list)
end


const DEFAULT_θstep = 2.0
const DEFAULT_l = [0, 2]
const DEFAULT_σ = 2
const DEFAULT_verbose = true


"""
    PBasexReconstructor

A callable object to reconstruct a 3D distribution from a projected image by
pBasex method.

Each object has its fixed size and is applicable to a square region of the
image.
"""
struct PBasexReconstructor
    G⁺::Array{Float64,2}
    len::Int
    θstep::Float64
    l::Array{Int,1}
    σ::Int
end

function PBasexReconstructor(len::Int, θstep::Float64, l, σ::Int, verbose::Bool)
    G = gmatrix(len, θstep, l, σ, verbose)
    G⁺ = pseudoinverse(G)
    G⁺ = balance(G⁺, len, θstep, l)
    PBasexReconstructor(G⁺, len, θstep, l, σ)
end

"""
    pbasex = PBasexReconstructor(len::Int; θstep=2.0, l=[0, 2], σ=2,
                                 verbose=true)

`len` is the size of the reconstructor. If `len` = 200, the reconstructor can
handle a square region of 200x200 pixels.

`θstep` is the angular step of polar grid for the reconstruction. Note that the
unit is degree. The angular grid is equally spaced from 0.0 through 180.0,
that is, 0.0:θstep:180.

`l` is the order of legendre polynomials included in the reconstruction.
In many cases, it will be even numbers like [0, 2], [0, 2, 4]. However, odd
numbers are also available.

Generally, building a reconstructor is a heavy calculation. Thus a progress bar
will be shown in the calculation. If `verbose` option is `false`, the progress
bar display will be suppressed.


The `pbasex` is a callable object.
"""
function PBasexReconstructor(len::Int; θstep=DEFAULT_θstep, l=DEFAULT_l,
                             σ=DEFAULT_σ, verbose=DEFAULT_verbose)
    PBasexReconstructor(len, Float64(θstep), l, Int(σ), verbose)
end


"""
    pbasex(img; base=0.0)

Reconstruct a 3D distribution from the projection `img`. It returns a
[`ReconstructedImage`](@ref).

The center of the image is set as `[size(img, 1)/2, size(img, 2)/2]`.

`base` keyword is employed to set a base-level of the image. The pixels darker
than the base-level will be set as the value. The negative pixels are
eliminated in default. If it is not required, pass `nothing`.
"""
function (rcs::PBasexReconstructor)(img, center, base)
    G⁺ = rcs.G⁺
    len = rcs.len
    θstep = rcs.θstep
    R, θ, R′, θ′ = polarcoordinates(len, θstep)
    l = rcs.l
    σ = rcs.σ

    Rmax = last(R)
    cropped, center = crop(img, center, Rmax)
    symmetrized = symmetrize(cropped, 2)
    P = cartesian_to_polar(symmetrized, center, R′, θ′)
    P = setbaselevel(P, 0.0)  # Eliminate negative pixels in a image array

    C = G⁺*P
    F = reconstruct(C, R, θ, l, σ)
    slice = polar_to_cartesian(F, R, θ)
    slice = setbaselevel(slice, base)
    I = radialdistribution(C, R, l, σ)
    β = anisotropyparameter(I, C, R, l, σ)
    ReconstructedImage(slice, F, β, I, R, θ, img, len, θstep, l, σ)
end

"""
    pbasex(img, center; base=0.0)

Reconstruct a 3D distribution from the projection `img` with the assigned
center.

# Example

``` julia-repl
julia> using PBasex

julia> pbasex = PBasexReconstructor(200)
PBasex.PBasexReconstructor[200x200]: l=[0, 2], σ=2

julia> ret = pbasex(img)
PBasex.Reconstructedimage: raw: 200x200, slice: 200x200
```


!!! info "Schematic of geometry"
```
-------------------------------------------------------------
| img                                                       |
|                      len                                  |
|               -----------------                           |
|               |               |                           |
|               |               |                           |
|               |       *       | len                       |
|               |     center    |                           |
|               |    (yc, xc)   |                           |
|               -----------------                           |
|                                                           |
|                 reconstructed                             |
|                     region                                |
|                                                           |
|                                                           |
|                                                           |
-------------------------------------------------------------
```
"""
function (rcs::PBasexReconstructor)(img, center; base=0.0)
    rcs(img, center, base)
end

function (rcs::PBasexReconstructor)(img; base=0.0)
    center = calccenter(img)
    rcs(img, center, base)
end


include("file.jl")
include("print.jl")


end # module
