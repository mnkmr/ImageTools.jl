import Base: show

function show(io::IO, ::MIME"text/plain", z::PBasexReconstructor)
    print("PBasex.PBasexReconstructor[$(z.len)x$(z.len)]:")
    print(" l=$(z.l)")
    print(", σ=$(z.σ)")
end


function show(io::IO, ::MIME"text/plain", z::ReconstructedImage)
    print("PBasex.ReconstructedImage:")
    h, w = size(z.raw)
    print(" raw: $(h)x$(w)")
    h, w = size(z.slice)
    print(", slice: $(h)x$(w)")
end
