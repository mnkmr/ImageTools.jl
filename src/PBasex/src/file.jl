import JLD2: @save, @load

# A representation of reconstructor for file storing with commpressed G⁺
struct ReconstructorFile
    cG⁺::Array{Float64,4}
    len::Int
    θstep::Float64
    l::Array{Int,1}
    σ::Int
end


# Compress G⁺ matrix by its symmetry to return a shrinked reconstructor
function compress(rcs::PBasexReconstructor)
    R, _, R′, θ′ = polarcoordinates(rcs.len, rcs.θstep)
    l = rcs.l
    imax = length(R′)
    jmax = length(θ′)
    kmax = length(R)
    jmid = ceil(Int, jmax/2)

    G⁺ = rcs.G⁺
    cG⁺ = zeros(Float64, imax, jmid, kmax, length(l))  # compressed G⁺
    for lidx in 1:length(l)
        sign = ifelse(iseven(l[lidx]), 1, -1)
        for k in 1:kmax
            kl = (k - 1)*length(l) + lidx
            for j in 1:jmid, i in 1:imax
                ij  = (i - 1)*jmax + j
                ij′ = (i - 1)*jmax + (jmax - j + 1)
                @inbounds cG⁺[i, j, k, lidx] = G⁺[kl, ij]
            end
        end
    end
    ReconstructorFile(cG⁺, rcs.len, rcs.θstep, rcs.l, rcs.σ)
end


# Decompress G⁺ matrix to return a reconstructor
function decompress(rcsfile::ReconstructorFile)
    R, _, R′, θ′ = polarcoordinates(rcsfile.len, rcsfile.θstep)
    l = rcsfile.l
    imax = length(R′)
    jmax = length(θ′)
    kmax = length(R)
    jmid = ceil(Int, jmax/2)

    cG⁺ = rcsfile.cG⁺
    G⁺ = zeros(Float64, kmax*length(l), imax*jmax)
    for lidx in 1:length(l)
        sign = ifelse(iseven(l[lidx]), 1, -1)
        for k in 1:kmax
            kl = (k - 1)*length(l) + lidx
            for j in 1:jmid, i in 1:imax
                ij  = (i - 1)*jmax + j
                ij′ = (i - 1)*jmax + (jmax - j + 1)
                @inbounds g⁺ = cG⁺[i, j, k, lidx]
                @inbounds G⁺[kl, ij] = g⁺
                @inbounds G⁺[kl, ij′] = sign*g⁺
            end
        end
    end
    PBasexReconstructor(G⁺, rcsfile.len, rcsfile.θstep, rcsfile.l, rcsfile.σ)
end


"""
    savercs(path::AbstractString, rcs::PBasexReconstructor)

Save the reconstructor to a file in JLD2 format.

The file can be loaded by [`loadrcs`](@ref)

# Example

```julia-repl
julia> using PBasex

julia> pbasex = PBasexReconstructor(200)   # Build a reconstructor
PBasex.PBasexReconstructor[200x200]: l=[0, 2], σ=2

julia> savercs("pbasex_200.jld2", pbasex)  # Save the reconstructor to a file

```
"""
function savercs(path::AbstractString, rcs::PBasexReconstructor)
    rcsfile = compress(rcs)
    @save path rcsfile
end


"""
    loadrcs(path::AbstractString)

Load a reconstructor from a file saved by [`savercs`](@ref).

# Example

```julia-repl
julia> using PBasex

julia> pbasex = loadrcs("pbasex_200.jld2")  # Load the reconstructor
PBasex.PBasexReconstructor[200x200]: l=[0, 2], σ=2

julia> ret = pbasex(img)  # Reconstruct an image
PBasex.Reconstructedimage: raw: 200x200, slice: 200x200
```
"""
function loadrcs(path::AbstractString)
    @load path rcsfile
    decompress(rcsfile)
end

# Search and load a reconstructor in a directory
# FIXME: This searching system is very slow, because we cannot know the
#        properties of reconstructor. I should replace the system for future.

function loadrcs(dir::AbstractString, limitlen::Int, θstep::Float64, l,
                 σ::Int, immediate::Bool=true)
    list = PBasexReconstructor[]
    for filename in sort(readdir(dir))
        if last(filename, 5) != ".jld2"
            continue
        end
        path = joinpath(dir, filename)
        if !isfile(path)
            continue
        end
        rcs = loadrcs(path)
        if !ismatched(rcs, limitlen, θstep, l, σ)
            continue
        end
        if immediate
            return rcs
        end
        push!(list, rcs)
    end
    largest(list)
end

function loadrcs(dir::AbstractString, limitlen::Int; θstep=DEFAULT_θstep,
                 l=DEFAULT_l, σ=DEFAULT_σ, immediate=false)
    loadrcs(dir, limitlen, Float64(θstep), l, σ, immediate)
end

function loadrcs(limitlen::Int; θstep=DEFAULT_θstep, l=DEFAULT_l, σ=DEFAULT_σ,
                 immediate=false)
    dir = cachedir()
    loadrcs(dir, limitlen, Float64(θstep), l, σ, immediate)
end


function ismatched(rcs::PBasexReconstructor, limitlen::Int, θstep::Float64,
                   l::Array{Int,1}, σ::Int)
    rcs.len ≤ limitlen && rcs.θstep == θstep && rcs.l == l && rcs.σ == σ
end


function largest(list::Array{PBasexReconstructor,1})
    if isempty(list)
        return nothing
    end
    sort(list, by = x -> x.len)
    last(list)
end


function cachedir()
    get(ENV, "PBASEX_CACHEDIR", "")
end
