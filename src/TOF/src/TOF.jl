module TOF

export simulatetof

import ..Photofragments: Photofragment, random_particle, I3D


const unit_t = 1e-9   # s

# Physical constants
const q  = 1.60217653e-19     # elementary charge
const NA = 6.0221415e23       # Avogadro number


struct Apparatus
    VR::Float64
    VL1::Float64
    VL2::Float64
    VG::Float64
    VD::Float64
    D1::Float64
    D2::Float64
    D3::Float64
    Drift::Float64

    function Apparatus(VR, VL1, VL2, VG, VD, D1, D2, D3, Drift)
        (VR < VL1) && error("'Apparatus' constructor: VR should be larger than VL1.")
        (VL1 < VL2) && error("'Apparatus' constructor: VL1 should be larger than VL2.")
        (VL2 < VG) && error("'Apparatus' constructor: VL2 should be larger than VG.")
        (VG < VD) && error("'Apparatus' constructor: VG should be larger than VD.")
        (D1 <= 0) && error("'Apparatus' constructor: D1 should be larger than 0.")
        (D2 <= 0) && error("'Apparatus' constructor: D2 should be larger than 0.")
        (D3 <= 0) && error("'Apparatus' constructor: D3 should be larger than 0.")
        (Drift <= 0) && error("'Apparatus' constructor: Drift should be larger than 0.")
        new(VR, VL1, VL2, VG, VD, D1, D2, D3, Drift)
    end
end

Apparatus(; VR=1200.0, VL1=1000.0, VL2=580.0, VG=0.0, VD=0.0, D1=22.0e-3, D2=26.0e-3, D3=32.0e-3, Drift=272e-3) = Apparatus(VR, VL1, VL2, VG, VD, D1, D2, D3, Drift)


function run(p::Photofragment, apparatus::Apparatus, v_beam)
    acceleration(p, V1, V2, d) = p.z*q*(V1 - V2)/(p.m*d)
    linearmotion(v0, a, d) = a == 0 ? d/v0 : (-v0 + sqrt(v0^2 + 2*a*d))/a

    v_recoil = p.v*cosd(p.Ω)
    v0 = v_beam + v_recoil

    # Between Repeller and L1
    a1 = acceleration(p, apparatus.VR, apparatus.VL1, apparatus.D1)
    t1 = linearmotion(v0, a1, 0.5*apparatus.D1)
    if t1 <= 0.0 return NaN end
    v1 = v0 + a1*t1

    # Between L1 and L2
    a2 = acceleration(p, apparatus.VL1, apparatus.VL2, apparatus.D2)
    t2 = linearmotion(v1, a2, apparatus.D2)
    v2 = v1 + a2*t2

    # Between L2 and Ground
    a3 = acceleration(p, apparatus.VL2, apparatus.VG, apparatus.D3)
    t3 = linearmotion(v2, a3, apparatus.D3)
    v3 = v2 + a3*t3

    # Inside drift tube
    a4 = acceleration(p, apparatus.VG, apparatus.VD, apparatus.Drift)
    t4 = linearmotion(v3, a4, apparatus.Drift)

    t1 + t2 + t3 + t4
end


function timerange(apparatus::Apparatus, m, v_beam, σv_beam, v_recoil, σv_recoil)
    v1 = v_beam - 3σv_beam
    v2 = v_beam + 3σv_beam
    v_recoil_max = v_recoil + 3σv_recoil
    p1 = Photofragment(m, 1, v_recoil_max, 180.0, 0.0)
    p2 = Photofragment(m, 1, v_recoil_max,   0.0, 0.0)
    t1 = run(p1, apparatus, v1)
    t2 = run(p2, apparatus, v2)
    tmin = unit_t*floor(min(t1, t2)/unit_t)
    tmax = unit_t*ceil(max(t1, t2)/unit_t)
    collect(tmin:unit_t:tmax)
end


const A0 = 1/sqrt(2π)


function simulatetof(n, apparatus::Apparatus, m, v_beam, σv_beam, v_recoil,
                     σv_recoil, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2)
    random_velocity(vmin, vmax) = vmin + (vmax - vmin)*rand(Float64)
    fv(v, vc, σv) = A0*exp(-(v - vc)^2/(2σv^2))
    index(t, tarray) = floor(Int, (t - tarray[1])/unit_t) + 1

    σv_beam = abs(σv_beam)
    σv_recoil = abs(σv_recoil)
    v_recoil_min, v_recoil_max = v_recoil - 3σv_recoil, v_recoil + 3σv_recoil
    v_beam_min, v_beam_max = v_beam - 3σv_beam, v_beam + 3σv_beam
    m = m*1e-3/NA
    tof = timerange(apparatus, m, v_beam, σv_beam, v_recoil, σv_recoil)
    spectrum = zeros(Float64, (length(tof), 2))
    spectrum[:, 1] = tof
    for i in 1:floor(Int, n)
        p = random_particle(m, 1, v_recoil_min, v_recoil_max)::Photofragment
        vb = random_velocity(v_beam_min, v_beam_max)
        t = run(p, apparatus, vb)

        idx = index(t, spectrum[:, 1])
        I_beam = fv(vb, v_beam, σv_beam)
        I_recoil = fv(p.v, v_recoil, σv_recoil)*
                   I3D(p.Ω, p.Θ, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2)*
                   sind(p.Ω)

        spectrum[idx, 2] += I_beam*I_recoil
    end
    spectrum[:, 2] ./= sum(spectrum[:, 2])
    spectrum
end

function simulatetof(n, VR, VL1, VL2, VG, VD, D1, D2, D3, Drift, m, v_beam,
                     σv_beam, v_recoil, σv_recoil, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2)
    appr = Apparatus(VR, VL1, VL2, VG, VD, D1, D2, D3, Drift)
    simulate(n, appr, m, v_beam, σv_beam, v_recoil, σv_recoil, Γ, Δ, Φ, α, χ,
             ϕ_μd, c1, c2)
end


# utility function to merge two TOF spectra
function merge(s1, f1, s2, f2)
    tmin = min(s1[1, 1], s2[1, 1]) - unit_t
    tmax = max(s1[end, 1], s2[end, 1]) + unit_t
    tof = collect(tmin:unit_t:tmax)
    merged = zeros(Float64, (length(tof), 4))
    merged[:, 1] = tof

    function migrate!(n, target, insertion, f)
        thr = unit_t/2
        i = findfirst(abs.(tof .- insertion[1, 1]) .< thr)
        for ins in insertion[:, 2]
            fraction = ins*f
            target[i, 2] += fraction
            target[i, n] = fraction
            i += 1
        end
    end
    migrate!(3, merged, s1, f1)
    migrate!(4, merged, s2, f2)
    merged
end


# wrapper function for easy testing
function execute(n, v_beam, σv_beam, v_recoil, σv_recoil, c1, c2)
    apprts = Apparatus(VR=1200.0, VL1=1000.0, VL2=580.0, VG=0.0, VD=0.0,
                       D1=22.0e-3, D2=26.0e-3, D3=32.0e-3, Drift=275.9e-3)
    NaturalAbundanceBr79 = 0.507
    NaturalAbundanceBr81 = 1.0 - NaturalAbundanceBr79
    spectrum79 = simulatetof(n, apprts, 79.0, v_beam, σv_beam, v_recoil,
                             σv_recoil, 0.0, 0.0, 0.0, 169.0, 10.5, 0.0, c1, c2)
    spectrum81 = simulatetof(n, apprts, 81.0, v_beam, σv_beam,
                             v_recoil*(79.0/81.0), σv_recoil*(79.0/81.0), 0.0,
                             0.0, 0.0, 169.0, 10.5, 0.0, c1, c2)
    spectrum = merge(spectrum79, NaturalAbundanceBr79,
                     spectrum81, NaturalAbundanceBr81)

    spectrum
end


end # module
