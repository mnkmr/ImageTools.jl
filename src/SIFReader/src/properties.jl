# Some properties are commented out because they cause error when trying to read
const Properties = Dict(
    :ATSIF_PROP_TYPE                    =>  "Type",
    :ATSIF_PROP_ACTIVE                  =>  "Active",
    :ATSIF_PROP_VERSION                 =>  "Version",
    :ATSIF_PROP_TIME                    =>  "Time",
    :ATSIF_PROP_FORMATTED_TIME          =>  "FormattedTime",
    :ATSIF_PROP_FILENAME                =>  "FileName",
    :ATSIF_PROP_TEMPERATURE             =>  "Temperature",
    :ATSIF_PROP_UNSTABILIZEDTEMPERATURE =>  "UnstabalizedTemperature",
    :ATSIF_PROP_HEAD                    =>  "Head",
    :ATSIF_PROP_HEADMODEL               =>  "HeadModel",
    :ATSIF_PROP_STORETYPE               =>  "StoreType",
    :ATSIF_PROP_DATATYPE                =>  "DataType",
    :ATSIF_PROP_SIDISPLACEMENT          =>  "SIDisplacement",
    :ATSIF_PROP_SINUMBERSUBFRAMES       =>  "SINumberSubFrames",
    :ATSIF_PROP_PIXELREADOUTTIME        =>  "PixelReadOutTime",
    :ATSIF_PROP_TRACKHEIGHT             =>  "TrackHeight",
    :ATSIF_PROP_READPATTERN             =>  "ReadPattern",
    :ATSIF_PROP_READPATTERN_FULLNAME    =>  "ReadPatternFullName",
    :ATSIF_PROP_SHUTTERDELAY            =>  "ShutterDelay",
    :ATSIF_PROP_CENTREROW               =>  "CentreRow",
    :ATSIF_PROP_ROWOFFSET               =>  "RowOffset",
    :ATSIF_PROP_OPERATION               =>  "Operation",
    :ATSIF_PROP_MODE                    =>  "Mode",
    :ATSIF_PROP_MODE_FULLNAME           =>  "ModeFullName",
    :ATSIF_PROP_TRIGGERSOURCE           =>  "TriggerSource",
    :ATSIF_PROP_TRIGGERSOURCE_FULLNAME  =>  "TriggerSourceFullName",
    :ATSIF_PROP_TRIGGERLEVEL            =>  "TriggerLevel",
    :ATSIF_PROP_EXPOSURETIME            =>  "ExposureTime",
    :ATSIF_PROP_DELAY                   =>  "Delay",
    :ATSIF_PROP_INTEGRATIONCYCLETIME    =>  "IntegrationCycleTime",
    :ATSIF_PROP_NUMBERINTEGRATIONS      =>  "NumberIntegrations",
    :ATSIF_PROP_KINETICCYCLETIME        =>  "KineticCycleTime",
    :ATSIF_PROP_FLIPX                   =>  "FlipX",
    :ATSIF_PROP_FLIPY                   =>  "FlipY",
    :ATSIF_PROP_CLOCK                   =>  "Clock",
    :ATSIF_PROP_ACLOCK                  =>  "AClock",
    :ATSIF_PROP_IOC                     =>  "IOC",
    :ATSIF_PROP_FREQUENCY               =>  "Frequency",
    :ATSIF_PROP_NUMBERPULSES            =>  "NumberPulses",
    :ATSIF_PROP_FRAMETRANSFERACQMODE    =>  "FrameTransferAcquisitionMode",
    :ATSIF_PROP_BASELINECLAMP           =>  "BaselineClamp",
    :ATSIF_PROP_PRESCAN                 =>  "PreScan",
    :ATSIF_PROP_EMREALGAIN              =>  "EMRealGain",
    :ATSIF_PROP_BASELINEOFFSET          =>  "BaselineOffset",
    :ATSIF_PROP_SWVERSION               =>  "SWVersion",
    :ATSIF_PROP_SWVERSIONEX             =>  "SWVersionEx",
    :ATSIF_PROP_MCP                     =>  "MCP",
    :ATSIF_PROP_GAIN                    =>  "Gain",
    :ATSIF_PROP_VERTICALCLOCKAMP        =>  "VerticalClockAmp",
    :ATSIF_PROP_VERTICALSHIFTSPEED      =>  "VerticalShiftSpeed",
    :ATSIF_PROP_OUTPUTAMPLIFIER         =>  "OutputAmplifier",
    :ATSIF_PROP_PREAMPLIFIERGAIN        =>  "PreAmplifierGain",
    :ATSIF_PROP_SERIAL                  =>  "Serial",
    :ATSIF_PROP_DETECTORFORMATX         =>  "DetectorFormatX",
    :ATSIF_PROP_DETECTORFORMATZ         =>  "DetectorFormatZ",
    :ATSIF_PROP_NUMBERIMAGES            =>  "NumberImages",
    :ATSIF_PROP_NUMBERSUBIMAGES         =>  "NumberSubImages",
    :ATSIF_PROP_SUBIMAGE_HBIN           =>  "SubImageHBin",
    :ATSIF_PROP_SUBIMAGE_VBIN           =>  "SubImageVBin",
    :ATSIF_PROP_SUBIMAGE_LEFT           =>  "SubImageLeft",
    :ATSIF_PROP_SUBIMAGE_RIGHT          =>  "SubImageRight",
    :ATSIF_PROP_SUBIMAGE_TOP            =>  "SubImageTop",
    :ATSIF_PROP_SUBIMAGE_BOTTOM         =>  "SubImageBottom",
    :ATSIF_PROP_BASELINE                =>  "Baseline",
    :ATSIF_PROP_CCD_LEFT                =>  "CCDLeft",
    :ATSIF_PROP_CCD_RIGHT               =>  "CCDRight",
    :ATSIF_PROP_CCD_TOP                 =>  "CCDTop",
    :ATSIF_PROP_CCD_BOTTOM              =>  "CCDBottom",
    :ATSIF_PROP_SENSITIVITY             =>  "Sensitivity",
    :ATSIF_PROP_DETECTIONWAVELENGTH     =>  "DetectionWavelength",
    :ATSIF_PROP_COUNTCONVERTMODE        =>  "CountConvertMode",
    :ATSIF_PROP_ISCOUNTCONVERT          =>  "IsCountConvert",
    :ATSIF_PROP_X_AXIS_TYPE             =>  "XAxisType",
    :ATSIF_PROP_X_AXIS_UNIT             =>  "XAxisUnit",
    :ATSIF_PROP_Y_AXIS_TYPE             =>  "YAxisType",
    # :ATSIF_PROP_Y_AXIS_UNIT             =>  "YAxisUnit",
    :ATSIF_PROP_Z_AXIS_TYPE             =>  "ZAxisType",
    :ATSIF_PROP_Z_AXIS_UNIT             =>  "ZAxisUnit",
    :ATSIF_PROP_USERTEXT                =>  "UserText",


    :ATSIF_PROP_ISPHOTONCOUNTINGENABLED =>  "IsPhotonCountingEnabled",
    :ATSIF_PROP_NUMBERTHRESHOLDS        =>  "NumberThresholds",
    :ATSIF_PROP_THRESHOLD1              =>  "Threshold1",
    :ATSIF_PROP_THRESHOLD2              =>  "Threshold2",
    :ATSIF_PROP_THRESHOLD3              =>  "Threshold3",
    :ATSIF_PROP_THRESHOLD4              =>  "Threshold4",

    :ATSIF_PROP_AVERAGINGFILTERMODE     =>  "AveragingFilterMode",
    :ATSIF_PROP_AVERAGINGFACTOR         =>  "AveragingFactor",
    :ATSIF_PROP_FRAMECOUNT              =>  "FrameCount",

    :ATSIF_PROP_NOISEFILTER             =>  "NoiseFilter",
    :ATSIF_PROP_THRESHOLD               =>  "Threshold",

    :ATSIF_PROP_TIME_STAMP              =>  "TimeStamp",
    # :ATSIF_PROP_METADATA_TICKS          =>  "Metadata",
    # :ATSIF_PROP_PRECISION_TIME_STAMP    =>  "HighPrecisionTimeStamp",
    # :ATSIF_PROP_CLOCK_FREQUENCY         =>  "ClockFrequency",

    :ATSIF_PROP_OUTPUTA_ENABLED         =>  "OutputAEnabled",
    :ATSIF_PROP_OUTPUTA_WIDTH           =>  "OutputAWidth",
    :ATSIF_PROP_OUTPUTA_DELAY           =>  "OutputADelay",
    :ATSIF_PROP_OUTPUTA_POLARITY        =>  "OutputAPolarity",
    :ATSIF_PROP_OUTPUTB_ENABLED         =>  "OutputBEnabled",
    :ATSIF_PROP_OUTPUTB_WIDTH           =>  "OutputBWidth",
    :ATSIF_PROP_OUTPUTB_DELAY           =>  "OutputBDelay",
    :ATSIF_PROP_OUTPUTB_POLARITY        =>  "OutputBPolarity",
    :ATSIF_PROP_OUTPUTC_ENABLED         =>  "OutputCEnabled",
    :ATSIF_PROP_OUTPUTC_WIDTH           =>  "OutputCWidth",
    :ATSIF_PROP_OUTPUTC_DELAY           =>  "OutputCDelay",
    :ATSIF_PROP_OUTPUTC_POLARITY        =>  "OutputCPolarity",
    :ATSIF_PROP_GATE_MODE               =>  "GateMode",
    :ATSIF_PROP_GATE_WIDTH              =>  "GateWidth",
    :ATSIF_PROP_GATE_DELAY              =>  "GateDelay",
    :ATSIF_PROP_GATE_DELAY_STEP         =>  "GateDelayStep",
    :ATSIF_PROP_GATE_WIDTH_STEP         =>  "GateWidthStep",
)

const SifDatasourceProperties = Dict(
    # Camera model
    :ATSIF_PROP_HEADMODEL               =>  "HeadModel",
    # Serial Number
    :ATSIF_PROP_SERIAL                  =>  "Serial",
    # Software version
    :ATSIF_PROP_SWVERSIONEX             =>  "SWVersionEx",
    # Unix time
    :ATSIF_PROP_TIME                    =>  "Time",
    # Time in String
    :ATSIF_PROP_FORMATTED_TIME          =>  "FormattedTime",
    # Camera temperature (celsius deg.)
    :ATSIF_PROP_TEMPERATURE             =>  "Temperature",
    # Acquisition mode
    :ATSIF_PROP_MODE_FULLNAME           =>  "ModeFullName",
    # Readout mode
    :ATSIF_PROP_READPATTERN_FULLNAME    =>  "ReadPatternFullName",
    # Trigger mode
    :ATSIF_PROP_TRIGGERSOURCE_FULLNAME  =>  "TriggerSourceFullName",
    # Exposure time (sec.)
    :ATSIF_PROP_EXPOSURETIME            =>  "ExposureTime",
    # Left edge of CCD (pixel)
    :ATSIF_PROP_CCD_LEFT                =>  "CCDLeft",
    # Right edge of CCD (pixel)
    :ATSIF_PROP_CCD_RIGHT               =>  "CCDRight",
    # Top edge of CCD (pixel)
    :ATSIF_PROP_CCD_TOP                 =>  "CCDTop",
    # Bottom edge of CCD (pixel)
    :ATSIF_PROP_CCD_BOTTOM              =>  "CCDBottom",
    # Horizontal binning
    :ATSIF_PROP_SUBIMAGE_HBIN           =>  "SubImageHBin",
    # Vertical binning
    :ATSIF_PROP_SUBIMAGE_VBIN           =>  "SubImageVBin",
    # Number of accumulations for each frame
    :ATSIF_PROP_NUMBERINTEGRATIONS      =>  "NumberIntegrations",
    # Number of frames
    :ATSIF_PROP_NUMBERIMAGES            =>  "NumberImages",
    # Number of sub-images in a frame
    :ATSIF_PROP_NUMBERSUBIMAGES         =>  "NumberSubImages",
    # Clockwise rotation (90 deg.)
    :ATSIF_PROP_CLOCK                   =>  "Clock",
    # Anti-Clockwise rotation (90 deg.)
    :ATSIF_PROP_ACLOCK                  =>  "AClock",
    # Horizontal flipping
    :ATSIF_PROP_FLIPX                   =>  "FlipX",
    # Vertical flipping
    :ATSIF_PROP_FLIPY                   =>  "FlipY",
    # EM Gain level
    :ATSIF_PROP_GAIN                    =>  "Gain",
    # Vertical shift time (sec.)
    :ATSIF_PROP_VERTICALSHIFTSPEED      =>  "VerticalShiftSpeed",
    # Pixel readout time
    :ATSIF_PROP_PIXELREADOUTTIME        =>  "PixelReadOutTime",
    # Pre-Amplifier Gain
    :ATSIF_PROP_PREAMPLIFIERGAIN        =>  "PreAmplifierGain",
    # Spurious Noise Filter Mode
    :ATSIF_PROP_NOISEFILTER             =>  "NoiseFilter",
    # Photon counting
    :ATSIF_PROP_ISPHOTONCOUNTINGENABLED =>  "IsPhotonCountingEnabled",
    # Data averaging filter
    :ATSIF_PROP_AVERAGINGFILTERMODE     =>  "AveragingFilterMode",
    # Data averaging factor
    :ATSIF_PROP_AVERAGINGFACTOR         =>  "AveragingFactor",

    # Types of each axis
    :ATSIF_PROP_X_AXIS_TYPE             =>  "XAxisType",
    :ATSIF_PROP_Y_AXIS_TYPE             =>  "YAxisType",
    :ATSIF_PROP_Z_AXIS_TYPE             =>  "ZAxisType",
)
