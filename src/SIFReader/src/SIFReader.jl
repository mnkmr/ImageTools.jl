module SIFReader

export readsif, readsifimg, hassource, countframe, countsubimg

using Libdl, Dates, Printf

include("finddll.jl")
include("constants.jl")
include("types.jl")
include("properties.jl")
include("print.jl")
include("errors.jl")


# Read a SIF format file to return a SifData typed value
function readsif(filename)
    sifopen(filename) do siflib
        sig = datasource_get(siflib, Signal)
        ref = datasource_get(siflib, Reference)
        bg = datasource_get(siflib, Background)
        live = datasource_get(siflib, Live)
        src = datasource_get(siflib, Source)
        sifdata = SifData(sig, ref, bg, live, src)
    end
end


# Read a SIF format file to return an image array
function readsifimg(sifdata::SifData, frameidx::Integer, subimgidx::Integer,
                    source, T::Type=Float64)::Array{T,2}
    ds = datasource_get(sifdata, source)
    if isempty(ds.frames)
        error_source_not_exist(source)
    end
    frame = ds.frames[frameidx]
    subimg = frame[subimgidx]::SifSubimage
    buffer_toArray(subimg.buffer, subimg.size, T)
end

function readsifimg(sifdata::SifData, frameidx::Integer=1,
                    subimgidx::Integer=1; source=Signal)
    readsifimg(sifdata, frameidx, subimgidx, source)
end

function readsifimg(filename, frameidx::Integer, subimgidx::Integer,
                    source, T::Type=Float64)
    sifopen(filename) do siflib
        if !ispresent(siflib, source)
            error_source_not_exist(source)
        end
        frame = frame_get(siflib, source, frameidx)
        subimg = frame[subimgidx]::SifSubimage
        buffer_toArray(subimg.buffer, subimg.size, T)
    end
end

function readsifimg(filename, frameidx::Integer=1, subimgidx::Integer=1;
                    source=Signal)
    readsifimg(filename, frameidx, subimgidx, source)
end


# Check if any data exists in a data source
function hassource(sifdata::SifData, source)
    ds = datasource_get(sifdata, source)
    !isempty(ds.frames)
end

function hassource(filename, source)
    sifopen(filename) do siflib
        ispresent(siflib, source)
    end
end


# Return the number of frames
function countframe(sifdata::SifData; source=Signal)
    ds = datasource_get(sifdata, source)
    if isempty(ds.frames)
        return 0
    end
    length(ds.frames)
end

function countframe(filename; source=Signal)
    sifopen(filename) do siflib
        if !ispresent(siflib, source)
            return 0
        end
        frame_num(siflib, source)
    end
end


# Return the number of sub-images
function countsubimg(sifdata::SifData; source=Signal)
    ds = datasource_get(sifdata, source)
    if isempty(ds.frames)
        return 0
    end
    length(ds.frames[1])
end

function countsubimg(filename; source=Signal)
    sifopen(filename) do siflib
        if !ispresent(siflib, source)
            return 0
        end
        subimage_num(siflib, source)
    end
end


# Set the range of reading data from a SIF format file
# readmode should be a number in ATSIF_ReadMode enumeration
function setreadmode(siflib, readmode)
    SetFileAccessMode = dlsym(siflib, :ATSIF_SetFileAccessMode)
    rc = ccall(SetFileAccessMode, UInt32, (Cuint,), readmode)
    returncode_check(rc)
end


# Check if any file is loaded on memory
function isloaded(siflib)
    IsLoaded = dlsym(siflib, :ATSIF_IsLoaded)
    isloaded = zeros(Cint, 1)
    rc = ccall(IsLoaded, UInt32, (Ptr{Cint},), isloaded)
    returncode_check(rc)
    isloaded[1] == 1
end


function _sif_open(siflib, filename::AbstractString)
    # NOTE: This function does not check the existence of filename
    if isloaded(siflib)
        return
    end
    ReadFromFile = dlsym(siflib, :ATSIF_ReadFromFile)
    rc = ccall(ReadFromFile, UInt32, (Cstring,), String(filename))
    returncode_check(rc)
end


function _sif_close(siflib)
    CloseFile = dlsym(siflib, :ATSIF_CloseFile)
    rc = ccall(CloseFile, UInt32, ())
    returncode_check(rc)
end


# Open ATSIFIO dll and a SIF format file for reading
# readmode should be a number in ATSIF_ReadMode enumeration
function sifopen(filename, readmode=ReadAll)
    if !isfile(filename)
        error_file_not_found(filename)
    end
    dllpath = finddll()
    siflib = dlopen(dllpath)
    setreadmode(siflib, readmode)
    _sif_open(siflib, filename)
    siflib
end

function sifopen(f::Function, filename, readmode=ReadAll)
    siflib = sifopen(filename, readmode)
    try
        f(siflib)
    finally
        sifclose(siflib)
    end
end


# Close both ATSIFIO dll and a SIF format file opened
function sifclose(siflib)
    _sif_close(siflib)
    dlclose(siflib)
end


# Check if any data present in a kind of data sources
# source should be a number in ATSIF_DataSource enumeration
function ispresent(siflib, source)
    IsDataSourcePresent = dlsym(siflib, :ATSIF_IsDataSourcePresent)
    ispresent = zeros(Cint, 1)
    rc = ccall(IsDataSourcePresent, UInt32, (Cuint, Ptr{Cint}), source, ispresent)
    returncode_check(rc)
    ispresent[1] == 1
end


# Return a version of a structure element (?)
# A structure element is specified by `element` which should be a number in
# ATSIF_StructureElement enumeration
function version(siflib, element)
    GetStructureVersion = dlsym(siflib, :ATSIF_GetStructureVersion)
    hi = zeros(Cuint, 1)
    lo = zeros(Cuint, 1)
    rc = ccall(GetStructureVersion, UInt32, (Cuint, Ptr{Cuint}, Ptr{Cuint}),
               element, hi, lo)
    returncode_check(rc)
    map(x -> Int(x[1]), (hi, lo))
end


# Return a property as a string
# See Properties in properties.jl for possible `name`s
function property_value(siflib, source, name)
    GetPropertyValue = dlsym(siflib, :ATSIF_GetPropertyValue)
    size = 128
    value = Array{Cchar,1}(undef, size)
    rc = ccall(GetPropertyValue, UInt32, (Cuint, Cstring, Ptr{Cchar}, Cuint),
               source, name, value, Cuint(size))
    returncode_check(rc)
    value[end] = 0
    unsafe_string(pointer(value))
end


# Return a integer to specify a type of a property
# See Properties in properties.jl for possible `name`s
# The correspondence of the integer and a type is described in
# ATSIF_PropertyType enumeration in properties.jl
function property_type(siflib, source, name)
    GetPropertyType = dlsym(siflib, :ATSIF_GetPropertyType)
    typ = zeros(Cuint, 1)
    rc = ccall(GetPropertyType, UInt32, (Cuint, Cstring, Ptr{Cuint}),
               source, name, typ)
    returncode_check(rc)
    Int(typ[1])
end


function property_parse(t, value)
    if t == ATSIF_AT_8
        parse(Cchar, value)
    elseif t == ATSIF_AT_U8
        parse(Cuchar, value)
    elseif t == ATSIF_AT_32
        parse(Cint, value)
    elseif t == ATSIF_AT_U32
        parse(Cuint, value)
    elseif t == ATSIF_AT_64
        parse(Clong, value)
    elseif t == ATSIF_AT_U64
        parse(Culong, value)
    elseif t == ATSIF_Float
        parse(Cfloat, value)
    elseif t == ATSIF_Double
        parse(Cdouble, value)
    elseif t == ATSIF_String
        String(value)
    else
        # should not reach here
        error_unknown_property_type(name, t)
    end
end


# Return a property
# source should be a number in ATSIF_DataSource enumeration
function property_get(siflib, source, name)
    value = property_value(siflib, source, name)
    t = property_type(siflib, source, name)
    property_parse(t, value)
end

function property_get(siflib, source, dict::Dict{Symbol,String})
    plist = map(values(dict)) do name
        (Symbol(name), property_get(siflib, source, name))
    end
    Dict(plist)
end


# Return the number of sub-images in a frames
# source should be a number in ATSIF_DataSource enumeration
function subimage_num(siflib, source)
    GetNumberSubImages = dlsym(siflib, :ATSIF_GetNumberSubImages)
    num = zeros(Cuint, 1)
    rc = ccall(GetNumberSubImages, UInt32, (Cuint, Ptr{Cuint}), source, num)
    returncode_check(rc, allow=[ATSIF_DATA_NOT_PRESENT])
    Int(num[1])
end


# Return the range and bininng information of a image
# source should be a number in ATSIF_DataSource enumeration
function subimage_info(siflib, source, i_julia)
    GetSubImageInfo = dlsym(siflib, :ATSIF_GetSubImageInfo)
    i_c = Cuint(i_julia - 1)
    left = zeros(Cuint, 1)
    bottom = zeros(Cuint, 1)
    right = zeros(Cuint, 1)
    top = zeros(Cuint, 1)
    hbin = zeros(Cuint, 1)
    vbin = zeros(Cuint, 1)
    rc = ccall(GetSubImageInfo, UInt32,
               (Cuint, Cuint, Ptr{Cuint}, Ptr{Cuint}, Ptr{Cuint}, Ptr{Cuint},
                Ptr{Cuint}, Ptr{Cuint}),
               source, i_c, left, bottom, right, top, hbin, vbin)
    returncode_check(rc, allow=[ATSIF_DATA_NOT_PRESENT])
    map(x -> Int(x[1]), (left, bottom, right, top, hbin, vbin))
end


# Return height and width of a sub-image
function subimage_size(left, bottom, right, top, hbin, vbin)
    if left == 0 || bottom == 0 || right == 0 || top == 0 || hbin == 0 || vbin == 0
        return 0, 0
    end
    height = (top - bottom + 1)÷vbin
    width = (right - left + 1)÷hbin
    height, width
end

function subimage_size(siflib, source, i)
    left, bottom, right, top, hbin, vbin = subimage_info(siflib, source, i)
    subimage_size(left, bottom, right, top, hbin, vbin)
end

function subimage_size(si::SifSubimage)
    subimage_size(si.info...)
end


# Return the total pixel number of a sub-image
# source should be a number in ATSIF_DataSource enumeration
function subimage_buffersize(siflib, source, i)
    height, width = subimage_size(siflib, source, i)
    height*width
end


# Return the total pixel number of all sub-images in a frame
# source should be a number in ATSIF_DataSource enumeration
function frame_buffersize(siflib, source)
    GetFrameSize = dlsym(siflib, :ATSIF_GetFrameSize)
    size = zeros(Cuint, 1)
    rc = ccall(GetFrameSize, UInt32, (Cuint, Ptr{Cuint}), source, size)
    returncode_check(rc, allow=[ATSIF_DATA_NOT_PRESENT])
    Int(size[1])
end


# Return the number of frames in a data source
# source should be a number in ATSIF_DataSource enumeration
function frame_num(siflib, source)
    GetNumberFrames = dlsym(siflib, :ATSIF_GetNumberFrames)
    num = zeros(Cuint, 1)
    rc = ccall(GetNumberFrames, UInt32, (Cuint, Ptr{Cuint}), source, num)
    returncode_check(rc, allow=[ATSIF_DATA_NOT_PRESENT])
    Int(num[1])
end


# Return a frame in a data source
# source should be a number in ATSIF_DataSource enumeration
function frame_get(siflib, source, i_julia)::Array{SifSubimage,1}
    GetFrame = dlsym(siflib, :ATSIF_GetFrame)
    fbufsize = frame_buffersize(siflib, source)
    fbuf = zeros(Cfloat, fbufsize)
    i_c = Cuint(i_julia - 1)
    rc = ccall(GetFrame, UInt32, (Cuint, Cuint, Ptr{Cfloat}, Cuint),
               source, i_c, fbuf, fbufsize)
    returncode_check(rc)

    sinum = subimage_num(siflib, source)
    sibufsizes = map(i -> subimage_buffersize(siflib, source, i), 1:sinum)
    siinfo = map(i -> subimage_info(siflib, source, i), 1:sinum)
    map(1:sinum) do i
        offset = sum(sibufsizes[1:(i-1)])
        chunksize = sibufsizes[i]
        sibuf = buffer_cut(fbuf, offset, chunksize)
        SifSubimage(sibuf, siinfo[i]...)
    end
end


# Return all frames in a data source
# source should be a number in ATSIF_DataSource enumeration
function frame_all(siflib, source)::Array{Array{SifSubimage,1},1}
    GetAllFrames = dlsym(siflib, :ATSIF_GetAllFrames)
    fnum = frame_num(siflib, source)
    fbufsize = frame_buffersize(siflib, source)
    totalsize = fbufsize*fnum
    totalbuffer = zeros(Cfloat, totalsize)
    rc = ccall(GetAllFrames, UInt32, (Cuint, Ptr{Cfloat}, Cuint), source,
               totalbuffer, totalsize)
    returncode_check(rc)

    sinum = subimage_num(siflib, source)
    sibufsizes = map(i -> subimage_buffersize(siflib, source, i), 1:sinum)
    siinfo = map(i -> subimage_info(siflib, source, i), 1:sinum)
    map(1:fnum) do i
        map(1:sinum) do j
            foffset = fbufsize*(i-1)
            sioffset = sum(sibufsizes[1:(j-1)])
            offset = foffset + sioffset
            chunksize = sibufsizes[j]
            sibuf = buffer_cut(totalbuffer, offset, chunksize)
            SifSubimage(sibuf, siinfo[j]...)
        end
    end
end


function datasource_get(siflib, source)
    name = datasource_name(source)
    if !ispresent(siflib, source)
        return SifDatasource(name)
    end
    frames = frame_all(siflib, source)
    prop = property_get(siflib, source, SifDatasourceProperties)
    SifDatasource(name, frames, prop)
end

function datasource_get(sifdata::SifData, source)
    name = datasource_name(source)
    getproperty(sifdata, name)
end


function datasource_name(source)
    if source == Signal
        :Signal
    elseif source == Reference
        :Reference
    elseif source == Background
        :Background
    elseif source == Live
        :Live
    elseif source == Source
        :Source
    else
        error_unknown_source(source)
    end
end


function buffer_cut(buffer, offset, chunksize)
    i = offset + 1
    j = offset + chunksize
    buffer[i:j]
end


function buffer_toArray(buffer, size, T)
    height, width = size
    if eltype(buffer) == T
        return permutedims(reshape(buffer, height, width))
    end
    convert.(T, permutedims(reshape(buffer, height, width)))
end


function returncode_check(rc; allow=[])
    if rc == ATSIF_SUCCESS
        return nothing
    end
    if rc in allow
        return nothing
    end
    error_in_calling_dll_function(rc)
end


end # module
