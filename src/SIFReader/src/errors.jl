# Errors

# ATSIFIO dll does not found
function error_dll_notfound()
    if Sys.WORD_SIZE == 64
        dllname = "ATSIFIO64.dll"
    else
        dllname = "ATSIFIO.dll"
    end
    error("SIFReader: $(dllname) not found")
end

function error_in_calling_dll_function(rc)
    error("SIFReader: error in calling a function of ATSIFIO library: $(rc)")
end

# File does not found
function error_file_not_found(filename)
    error("SIFReader: opening file $(filename): No such file or directory")
end

# Data source does not exist
function error_source_not_exist(source)
    src = datasource_name(source)
    error("SIFReader: Data source $(src) does not exits")
end

# Unknown data source specified
function error_unknown_source(source="")
    error("SIFReader: Unknown data source $(source)")
end

# Unknown property type
function error_unknown_property_type(name, t)
    error("SIFReader: unknown property type with $(name): $(t)")
end
