module Photofragments


export Photofragment, I3D, random_particle


"""
    Photofragment(m, z, v, Ω, Θ, P)

Type for a photofragment particle, mainly for Monte Carlo simulations.
It has mass(m), charge(z), and velocity(v, Ω, Θ).
"""
struct Photofragment
    m::Float64
    z::Int
    v::Float64
    Ω::Float64
    Θ::Float64
end


"""
    I3D(Ω, Θ, Γ, Δ, Φ, α, χ, ϕ, c1, c2)

Calculate 3-dimensional photofragment distribution.
Check [Rakitzis et al., Chem. Phys. Lett. 372 (2003) 187-194] for details.
Calcuate a photofragment distribution in laboratory frame,
based on Eq.(8) and Eq.(9a)-(9d) in the Ref.
"""
function I3D(Ω, Θ, Γ, Δ, Φ, α, χ, ϕ, c₁, c₂)
    cosΩ = cosd(Ω)
    sinΩ = sind(Ω)
    sin²Ω = sinΩ*sinΩ

    cosΓ = cosd(Γ)
    sinΓ = sind(Γ)

    cosΘ = cosd(Θ)
    sinΘ = sind(Θ)

    cosΔ = cosd(Δ)
    sinΔ = sind(Δ)

    cosΦ = cosd(Φ)
    sinΦ = sind(Φ)

    cosα = cosd(α)
    cos²α = cosα*cosα
    sinα = sind(α)
    sin²α = sinα*sinα

    cosχ = cosd(χ)
    cos²χ = cosχ*cosχ
    sinχ = sind(χ)
    sin²χ = sinχ*sinχ

    cosϕ = cosd(ϕ)
    cos²ϕ = cosϕ*cosϕ
    sinϕ = sind(ϕ)
    sin²ϕ = sinϕ*sinϕ

    P₂χ = 0.5*(3*cos²χ - 1)
    P₂α = 0.5*(3*cos²α - 1)

    A = cosΩ*cosΓ + sinΩ*sinΓ*cosΘ
    A² = A*A

    B = cosΩ*cosΔ + sinΩ*sinΔ*cosd(Θ - Φ)
    B² = B*B

    C = (sin²Ω*cosΓ*cosΔ + sinΓ*sinΔ*cosΦ -
         sinΩ*cosΩ*(sinΔ*cosΓ*cosd(Φ - Θ) + sinΓ*cosΔ*cosΘ) -
         sin²Ω*sinΓ*sinΔ*cosd(Φ - Θ)*cosΘ)
    C² = C*C

    D = cosΩ*sinΓ*sinΔ*sinΦ - sinΩ*(sinΔ*cosΓ*sind(Φ - Θ) + sinΓ*cosΔ*sinΘ)
    D² = D*D

    ((1 + P₂χ*(3*A² - 1))*(1 + 2*c₁*cosα*B + c₂*P₂α*(3*B² - 1)) +
     6*A*sinχ*cosχ*sinα*(C*cosϕ - D*sinϕ)*(c₁ + 3*c₂*cosα*B) +
     9/8*c₂*sin²χ*sin²α*((cos²ϕ - sin²ϕ)*(C² - D²) - 4*C*D*sinϕ*cosϕ))
end


const A0 = 1/sqrt(2π)


"""
    random_particle(m, z, vmin, vmax)

Generate a photofragment recoiling toward a random direction.
Give mass(m), charge(z), the minimum and maximam limit of velocity.
"""
function random_particle(m, z, vmin, vmax)
    v = vmin + (vmax - vmin)*rand(Float64)
    Ω = 180*rand(Float64)
    Θ = 360*rand(Float64)
    Photofragment(m, z, v, Ω, Θ)
end


end # module
