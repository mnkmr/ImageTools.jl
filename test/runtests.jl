using Test
using ImageTools

@testset "Ionimage" begin
    @test simulateimg(x -> 1, 100, 200, 100, 90.0, 180.0, 0.0,
                      180.0, 0.0, 0.0, 0.0, 0.0,
                      (501, 501), (251, 251), 1.0) isa Matrix{Float64}

    @test simulateimg(x -> 1, 100, 200, 100, 90.0, 180.0, 0.0,
                      180.0, 0.0, 0.0, 0.0, 0.0,
                      (501, 501), (251, 251), 0.1) isa Matrix{Float64}

    @test simulateimg(x -> exp(-(x - 150)^2/(2*3^2)), 100, 200, 100) isa Matrix{Float64}
    @test simulateimg(x -> exp(-(x - 150)^2/(2*3^2)), 100, 200, 100; slice=0.1) isa Matrix{Float64}

    @test simulateimg(200, 5, 100) isa Matrix{Float64}
    @test simulateimg(200, 5, 100; slice=0.1) isa Matrix{Float64}
end

@testset "TOF" begin
    apprts = TOF.Apparatus()
    @test simulatetof(100, apprts, 28, 800.0, 20.0, 1000.0, 20.0,
                      90.0, 180.0, 0.0, 180.0, 0.0, 0.0, 0.0, 0.0) isa Matrix{Float64}
end
